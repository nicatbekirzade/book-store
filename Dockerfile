FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/book-store-1.0..jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/book-store-1.0..jar"]
