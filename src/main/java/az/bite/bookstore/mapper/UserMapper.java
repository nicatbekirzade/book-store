package az.bite.bookstore.mapper;

import az.bite.bookstore.config.ModelMapperConfig;
import az.bite.bookstore.domain.User;
import az.bite.bookstore.service.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.context.annotation.Import;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel="spring", unmappedTargetPolicy = IGNORE)
public interface UserMapper {

    UserDto userToDto(User user);

    @Mapping(target = "password", ignore = true)
    User dtoToUser(UserDto dto);

}
