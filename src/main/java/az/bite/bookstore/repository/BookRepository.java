package az.bite.bookstore.repository;

import az.bite.bookstore.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long>,
        JpaSpecificationExecutor<Book> {


    Optional<Object> findBookByNameIgnoreCase(String name);

    List<Book> findAllByAuthorsId(Long id);
}
