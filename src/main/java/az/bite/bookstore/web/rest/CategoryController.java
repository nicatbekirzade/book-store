package az.bite.bookstore.web.rest;

import az.bite.bookstore.service.CategoryService;
import az.bite.bookstore.service.dto.AuthorDto;
import az.bite.bookstore.service.dto.CategoryDto;
import az.bite.bookstore.service.dto.GenericSearchDto;
import az.bite.bookstore.web.rest.dto.AuthorRequestDto;
import az.bite.bookstore.web.rest.dto.CategoryRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/v1/category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @PostMapping
    public CategoryDto createCategory(@RequestBody @Valid CategoryRequestDto requestDto) {
        return categoryService.create(requestDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CategoryDto> update(@PathVariable Long id,
                                            @RequestBody CategoryRequestDto  requestDto) {
        return ResponseEntity.ok(categoryService.update(id, requestDto));
    }

    @PostMapping("/search")
    public Page<CategoryDto> search(@RequestBody GenericSearchDto dto, Pageable pageable) {
        log.trace("Search category request");
        return categoryService.search(dto, pageable);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CategoryDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(categoryService.getById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.trace("Delete category");

        categoryService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
