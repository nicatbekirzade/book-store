package az.bite.bookstore.web.rest;

import az.bite.bookstore.service.BookService;
import az.bite.bookstore.service.dto.BookDto;
import az.bite.bookstore.service.dto.GenericSearchDto;
import az.bite.bookstore.web.rest.dto.BookRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @PostMapping
    public BookDto createBook(@RequestBody @Valid BookRequestDto requestDto) {
        return bookService.createBook(requestDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BookDto> update(@PathVariable Long id,
                                          @RequestBody BookRequestDto requestDto) {
        return ResponseEntity.ok(bookService.update(id, requestDto));
    }

    @PostMapping("/search")
    public Page<BookDto> search(@RequestBody GenericSearchDto dto, Pageable pageable) {
        log.trace("Search book request");
        return bookService.search(dto, pageable);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(bookService.getById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.trace("Delete book");

        bookService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/list")
    public List<BookDto> listAllBooks() {
        return bookService.listAllBooks();
    }

    @GetMapping("/author/{id}")
    public List<BookDto> listAuthorsBooks(@PathVariable Long id) {
        return bookService.listAuthorsBooks(id);
    }

}
