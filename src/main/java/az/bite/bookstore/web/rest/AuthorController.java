package az.bite.bookstore.web.rest;

import az.bite.bookstore.service.AuthorService;
import az.bite.bookstore.service.dto.AuthorDto;
import az.bite.bookstore.service.dto.GenericSearchDto;
import az.bite.bookstore.web.rest.dto.AuthorRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/v1/author")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

//    @GetMapping("/hello")
//    public String get() {
//        return "hello   arsghergggggggggggggggggggggg";
//    }

    @PostMapping
    public AuthorDto createAuthor(@RequestBody @Valid AuthorRequestDto requestDto) {
        return authorService.create(requestDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AuthorDto> update(@PathVariable Long id,
                                            @RequestBody AuthorRequestDto requestDto) {
        return ResponseEntity.ok(authorService.update(id, requestDto));
    }

    @PostMapping("/search")
    public Page<AuthorDto> search(@RequestBody GenericSearchDto dto, Pageable pageable) {
        log.trace("Search author request");
        return authorService.search(dto, pageable);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(authorService.getById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.trace("Delete author");

        authorService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
