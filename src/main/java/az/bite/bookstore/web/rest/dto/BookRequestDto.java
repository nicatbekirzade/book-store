package az.bite.bookstore.web.rest.dto;

import az.bite.bookstore.domain.enums.BookStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookRequestDto {

    @NotBlank
    private String title;

    @NotBlank
    private String name;

    @NotBlank
    private String bookDescription;

    @NotBlank
    private int page;

    @Valid
    private BookStatus status;

    private Long category;

    @Size(min = 1)
    private Set<Long> authors;

}
