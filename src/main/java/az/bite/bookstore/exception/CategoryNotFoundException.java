package az.bite.bookstore.exception;

public class CategoryNotFoundException extends NotFoundException {
    public static final String CATEGORY_NOT_FOUND = "Category Not Found";

    private static final long serialVersionUID = 584456332465811L;

    public CategoryNotFoundException(String name) {
        super(String.format("Category \"%s\" not found", name));
    }

    public CategoryNotFoundException(Long id) {
        super(String.format("Category with given id: \"%s\" not found", id));
    }

    public CategoryNotFoundException(String param, String email) {
        super(String.format("Category with \"%s\" - \"%s\" not found", param, email));
    }

    public CategoryNotFoundException() {
        super(CATEGORY_NOT_FOUND);
    }
}
