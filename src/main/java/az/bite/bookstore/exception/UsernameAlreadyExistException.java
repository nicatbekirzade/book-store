package az.bite.bookstore.exception;

public class UsernameAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 1L;

    public UsernameAlreadyExistException(String username) {
        super(String.format("Username \"%s\" already exist ", username));
    }
}
