package az.bite.bookstore.exception;

public class BookNameAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 2L;

    public BookNameAlreadyExistException(String bookName) {
        super(String.format("Book Name \"%s\" already exist ", bookName));
    }
}
