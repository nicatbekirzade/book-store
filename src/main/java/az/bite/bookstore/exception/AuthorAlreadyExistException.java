package az.bite.bookstore.exception;

public class AuthorAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 2L;

    public AuthorAlreadyExistException(String author) {
        super(String.format("Author \"%s\" already exist ", author));
    }
}
