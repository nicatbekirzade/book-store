package az.bite.bookstore.exception;

public class CategoryAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 34523L;

    public CategoryAlreadyExistException(String category) {
        super(String.format("Category \"%s\" already exist ", category));
    }
}
