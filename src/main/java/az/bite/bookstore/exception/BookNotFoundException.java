package az.bite.bookstore.exception;

public class BookNotFoundException extends NotFoundException {
    public static final String BOOK_NOT_FOUND = "Book Not Found";

    private static final long serialVersionUID = 58432124465811L;

    public BookNotFoundException(String name) {
        super(String.format("User \"%s\" not found", name));
    }

    public BookNotFoundException(Long id) {
        super(String.format("Book with given id: \"%s\" not found", id));
    }

    public BookNotFoundException(String param, String email) {
        super(String.format("Book with \"%s\" - \"%s\" not found", param, email));
    }

    public BookNotFoundException() {
        super(BOOK_NOT_FOUND);
    }
}
