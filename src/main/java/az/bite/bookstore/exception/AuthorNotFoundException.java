package az.bite.bookstore.exception;

public class AuthorNotFoundException extends NotFoundException {
    public static final String AUTHOR_NOT_FOUND = "Author Not Found";

    private static final long serialVersionUID = 58432132465811L;

    public AuthorNotFoundException(String name) {
        super(String.format("Author \"%s\" not found", name));
    }

    public AuthorNotFoundException(Long id) {
        super(String.format("Author with given id: \"%s\" not found", id));
    }

    public AuthorNotFoundException(String param, String email) {
        super(String.format("Author with \"%s\" - \"%s\" not found", param, email));
    }

    public AuthorNotFoundException() {
        super(AUTHOR_NOT_FOUND);
    }
}
