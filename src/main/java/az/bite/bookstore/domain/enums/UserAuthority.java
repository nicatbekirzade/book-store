package az.bite.bookstore.domain.enums;

public enum UserAuthority {
    USER, ADMIN, SUPER_USER, ANONYMOUS, AGENT, PUBLISHER
}
