package az.bite.bookstore.domain.enums;

public enum BookStatus {
    ACTIVE, DELETED
}
