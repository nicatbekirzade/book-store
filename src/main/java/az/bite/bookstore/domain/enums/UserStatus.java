package az.bite.bookstore.domain.enums;

public enum UserStatus {
    ACTIVE, DELETED, BLOCKED
}
