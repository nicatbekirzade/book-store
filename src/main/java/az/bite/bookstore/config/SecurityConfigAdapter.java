package az.bite.bookstore.config;

import az.bite.bookstore.domain.enums.UserAuthority;
import az.bite.bookstore.service.security.MyUserDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RequiredArgsConstructor
@Configuration
public class SecurityConfigAdapter extends WebSecurityConfigurerAdapter {

    private static final String BOOK_PATH = "/v1/book/**";
    private static final String CATEGORY_PATH = "/v1/category/**";
    private static final String CATEGORY_SEARCH_PATH = "/v1/category/search/";
    private static final String ADMIN = UserAuthority.ADMIN.name();
    private static final String USER = UserAuthority.USER.name();

    @Bean
    public UserDetailsService userDetailsService() {
        return new MyUserDetailService();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/v1/user/**").permitAll()
                .antMatchers(CATEGORY_SEARCH_PATH).permitAll()

//                .antMatchers(HttpMethod.GET, CATEGORY_PATH).hasAnyAuthority(USER, ADMIN)
                .antMatchers(HttpMethod.POST, CATEGORY_PATH).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.DELETE, CATEGORY_PATH).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.PUT, CATEGORY_PATH).hasAnyAuthority(ADMIN)

                .antMatchers(HttpMethod.GET, BOOK_PATH).hasAnyAuthority(USER, ADMIN)
                .antMatchers(HttpMethod.POST, BOOK_PATH).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.DELETE, BOOK_PATH).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.PUT, BOOK_PATH).hasAnyAuthority(ADMIN, USER)
                .antMatchers("/assign/**").hasAuthority(ADMIN)
                .antMatchers("/owner/**").hasAuthority(ADMIN)
                .anyRequest().authenticated()
                .and().csrf().disable()
                .formLogin().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("admin").password("{noop}123").roles("ADMIN");

        auth
                .userDetailsService(userDetailsService())
                .passwordEncoder(bCryptPasswordEncoder());
    }

}
