package az.bite.bookstore.service.impl;

import az.bite.bookstore.domain.Authority;
import az.bite.bookstore.domain.User;
import az.bite.bookstore.domain.enums.UserAuthority;
import az.bite.bookstore.exception.UserNotFoundException;
import az.bite.bookstore.exception.UsernameAlreadyExistException;
import az.bite.bookstore.mapper.UserMapper;
import az.bite.bookstore.repository.UserRepository;
import az.bite.bookstore.service.UserService;
import az.bite.bookstore.service.auth.SecurityService;
import az.bite.bookstore.service.dto.UserDto;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    private final SecurityService securityService;

    @Override
    @Transactional
    public void  createUser(UserDto userVm) {
        userRepository.findUserByUsernameIgnoreCase(userVm.getUsername())
                .ifPresent(user -> {
                    throw new UsernameAlreadyExistException(userVm.getUsername());
                });
        User user = createUserEntityObject(userVm, createAuthorities(UserAuthority.AGENT.name()));
        System.out.println("create");
        userRepository.save(user);
    }

    @Override
    public UserDto getCurrentUser() {
        return securityService.getCurrentUserLogin()
                .map(userRepository::findUserByUsernameIgnoreCase)
                .map(Optional::get)
                .map(userMapper::userToDto)
                .orElseThrow(UserNotFoundException::new);
    }

    private Set<Authority> createAuthorities(String... authoritiesString) {
        Set<Authority> authorities = new HashSet<>();
        for (String authorityString : authoritiesString) {
            authorities.add(new Authority(authorityString));
        }
        return authorities;
    }

    private User createUserEntityObject(UserDto userVm, Set<Authority> authorities) {
        User user = userMapper.dtoToUser(userVm);
        user.setPassword(passwordEncoder.encode(userVm.getPassword()));
        user.setAuthorities(authorities);
        return user;
    }

}
