package az.bite.bookstore.service.impl;

import az.bite.bookstore.domain.Author;
import az.bite.bookstore.exception.AuthorAlreadyExistException;
import az.bite.bookstore.exception.AuthorNotFoundException;
import az.bite.bookstore.exception.BookNameAlreadyExistException;
import az.bite.bookstore.repository.AuthorRepository;
import az.bite.bookstore.search.SearchSpecification;
import az.bite.bookstore.service.AuthorService;
import az.bite.bookstore.service.dto.AuthorDto;
import az.bite.bookstore.service.dto.GenericSearchDto;
import az.bite.bookstore.web.rest.dto.AuthorRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final ModelMapper mapper;

    @Override
    public AuthorDto create(AuthorRequestDto requestDto) {
        authorRepository.findByFirstNameIgnoreCase(requestDto.getFirstName())
                .ifPresent(book -> {
                    throw new AuthorAlreadyExistException(requestDto.getFirstName());
                });
        Author author = mapper.map(requestDto, Author.class);
        authorRepository.save(author);
        return mapper.map(author, AuthorDto.class);
    }

    @Override
    public AuthorDto update(Long id, AuthorRequestDto requestDto) {
        log.trace("Update author with id {} to {}", id, requestDto);
        return authorRepository.findById(id)
                .map(author -> {
                    mapper.map(requestDto, author);
                    author.setId(id);
                    return mapper.map(authorRepository.save(author), AuthorDto.class);
                }).orElseThrow(() -> new AuthorNotFoundException(id));
    }

    @Override
    public AuthorDto getById(Long id) {
        return authorRepository.findById(id)
                .map(author -> mapper.map(author, AuthorDto.class))
                .orElseThrow(() -> new AuthorNotFoundException(id));
    }

    @Override
    public void delete(Long id) {
        authorRepository.delete(authorRepository.findById(id)
                .orElseThrow(() -> new AuthorNotFoundException(id)));
    }

    @Override
    public Page<AuthorDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return authorRepository
                .findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(author -> mapper.map(author, AuthorDto.class));
    }

}
