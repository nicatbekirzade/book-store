package az.bite.bookstore.service.impl;

import az.bite.bookstore.domain.Author;
import az.bite.bookstore.domain.Book;
import az.bite.bookstore.domain.Category;
import az.bite.bookstore.exception.AuthorNotFoundException;
import az.bite.bookstore.exception.BookNameAlreadyExistException;
import az.bite.bookstore.exception.BookNotFoundException;
import az.bite.bookstore.exception.CategoryNotFoundException;
import az.bite.bookstore.repository.AuthorRepository;
import az.bite.bookstore.repository.BookRepository;
import az.bite.bookstore.repository.CategoryRepository;
import az.bite.bookstore.search.SearchSpecification;
import az.bite.bookstore.service.BookService;
import az.bite.bookstore.service.dto.BookDto;
import az.bite.bookstore.service.dto.GenericSearchDto;
import az.bite.bookstore.web.rest.dto.BookRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final CategoryRepository categoryRepository;
    private final AuthorRepository authorRepository;
    private final ModelMapper mapper;

    @Override
    public BookDto createBook(BookRequestDto requestDto) {
        bookRepository.findBookByNameIgnoreCase(requestDto.getName())
                .ifPresent(book -> {
                    throw new BookNameAlreadyExistException(requestDto.getName());
                });
        Book book = mapper.map(requestDto, Book.class);
        book.setAuthors(getAuthorList(requestDto.getAuthors()));
        book.setCategory(getCategory(requestDto.getCategory()));
        bookRepository.save(book);
        return mapper.map(book, BookDto.class);
    }

    @Override
    public BookDto update(Long id, BookRequestDto requestDto) {
        log.trace("Update book with id {} to {}", id, requestDto);
        return bookRepository.findById(id)
                .map(book -> {
                    mapper.map(requestDto, book);
                    book.setAuthors(getAuthorList(requestDto.getAuthors()));
                    book.setId(id);
                    return mapper.map(bookRepository.save(book), BookDto.class);
                }).orElseThrow(() -> new BookNotFoundException(id));
    }

    @Override
    public BookDto getById(Long id) {
        return bookRepository.findById(id)
                .map(book -> mapper.map(book, BookDto.class))
                .orElseThrow(() -> new BookNotFoundException(id));
    }

    @Override
    public void delete(Long id) {
        bookRepository.delete(bookRepository.findById(id)
                .orElseThrow(() -> new BookNotFoundException(id)));
    }

    @Override
    public Page<BookDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return bookRepository
                .findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(book -> mapper.map(book, BookDto.class));
    }

    @Override
    public List<BookDto> listAllBooks() {
        return bookRepository.findAll()
                .stream()
                .map(book -> mapper.map(book, BookDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<BookDto> listAuthorsBooks(Long id) {
        return bookRepository.findAllByAuthorsId(id)
                .stream()
                .map(book -> mapper.map(book, BookDto.class))
                .collect(Collectors.toList());
    }

    private Set<Author> getAuthorList(Set<Long> authors) {
        return authors.stream()
                .map(id -> authorRepository.findById(id)
                        .orElseThrow(() -> new AuthorNotFoundException(id)))
                .collect(Collectors.toSet());
    }

    private Category getCategory(Long category) {
        return categoryRepository.findById(category)
                .orElseThrow(() -> new CategoryNotFoundException(category));
    }
}
