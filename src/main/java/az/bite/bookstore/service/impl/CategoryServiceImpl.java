package az.bite.bookstore.service.impl;

import az.bite.bookstore.domain.Category;
import az.bite.bookstore.exception.BookNameAlreadyExistException;
import az.bite.bookstore.exception.CategoryAlreadyExistException;
import az.bite.bookstore.exception.CategoryNotFoundException;
import az.bite.bookstore.repository.CategoryRepository;
import az.bite.bookstore.search.SearchSpecification;
import az.bite.bookstore.service.CategoryService;
import az.bite.bookstore.service.dto.CategoryDto;
import az.bite.bookstore.service.dto.GenericSearchDto;
import az.bite.bookstore.web.rest.dto.CategoryRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final ModelMapper mapper;

    @Override
    public CategoryDto create(CategoryRequestDto requestDto) {
        categoryRepository.findByNameIgnoreCase(requestDto.getName())
                .ifPresent(book -> {
                    throw new CategoryAlreadyExistException(requestDto.getName());
                });
        Category category = mapper.map(requestDto, Category.class);
        categoryRepository.save(category);
        return mapper.map(category, CategoryDto.class);
    }

    @Override
    public CategoryDto update(Long id, CategoryRequestDto requestDto) {
        log.trace("Update category with id {} to {}", id, requestDto);
        return categoryRepository.findById(id)
                .map(category -> {
                    mapper.map(requestDto, category);
                    category.setId(id);
                    return mapper.map(categoryRepository.save(category), CategoryDto.class);
                }).orElseThrow(() -> new CategoryNotFoundException(id));
    }

    @Override
    public CategoryDto getById(Long id) {
        return categoryRepository.findById(id)
                .map(author -> mapper.map(author, CategoryDto.class))
                .orElseThrow(() -> new CategoryNotFoundException(id));
    }

    @Override
    public void delete(Long id) {
        categoryRepository.delete(categoryRepository.findById(id)
                .orElseThrow(() -> new CategoryNotFoundException(id)));
    }

    @Override
    public Page<CategoryDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return categoryRepository
                .findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(author -> mapper.map(author, CategoryDto.class));
    }

}
