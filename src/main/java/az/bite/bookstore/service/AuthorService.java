package az.bite.bookstore.service;

import az.bite.bookstore.service.dto.AuthorDto;
import az.bite.bookstore.service.dto.GenericSearchDto;
import az.bite.bookstore.web.rest.dto.AuthorRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AuthorService {

    AuthorDto create(AuthorRequestDto requestDto);

    AuthorDto update(Long id, AuthorRequestDto requestDto);

    AuthorDto getById(Long id);

    void delete(Long id);

    Page<AuthorDto> search(GenericSearchDto dto, Pageable pageable);
}
