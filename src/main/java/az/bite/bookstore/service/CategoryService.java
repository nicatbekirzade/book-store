package az.bite.bookstore.service;

import az.bite.bookstore.service.dto.CategoryDto;
import az.bite.bookstore.service.dto.GenericSearchDto;
import az.bite.bookstore.web.rest.dto.CategoryRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoryService {

    CategoryDto create(CategoryRequestDto requestDto);

    CategoryDto update(Long id, CategoryRequestDto requestDto);

    CategoryDto getById(Long id);

    void delete(Long id);

    Page<CategoryDto> search(GenericSearchDto dto, Pageable pageable);
}
