package az.bite.bookstore.service;

import az.bite.bookstore.service.dto.BookDto;
import az.bite.bookstore.service.dto.GenericSearchDto;
import az.bite.bookstore.web.rest.dto.BookRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookService {

    BookDto createBook(BookRequestDto requestDto);

    BookDto update(Long id, BookRequestDto requestDto);

    BookDto getById(Long id);

    void delete(Long id);

    Page<BookDto> search(GenericSearchDto dto, Pageable pageable);

    List<BookDto> listAllBooks();

    List<BookDto> listAuthorsBooks(Long id);
}
