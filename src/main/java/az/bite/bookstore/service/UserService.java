package az.bite.bookstore.service;

import az.bite.bookstore.service.dto.UserDto;

public interface UserService {

    void createUser(UserDto mUserDto);

    UserDto getCurrentUser();

}
