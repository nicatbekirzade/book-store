package az.bite.bookstore.service.security;

import az.bite.bookstore.domain.Authority;
import az.bite.bookstore.domain.User;
import az.bite.bookstore.domain.enums.UserStatus;
import az.bite.bookstore.exception.NotFoundException;
import az.bite.bookstore.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = findUserByUserName(userName);
        List<GrantedAuthority> authorities = getUserAuthority(user.getAuthorities());
        return buildUserForAuthentication(user, authorities);
    }

    private List<GrantedAuthority> getUserAuthority(Set<Authority> userRoles) {
        Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
        for (Authority authority : userRoles) {
            roles.add(new SimpleGrantedAuthority(authority.getName()));
        }
        return new ArrayList<>(roles);
    }

    private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                user.getStatus().equals(UserStatus.ACTIVE), true, true, true, authorities);
    }

    private User findUserByUserName(String userName) {
        return userRepository.findByUsername(userName).orElseThrow(() ->
                new NotFoundException(String.format(
                        "User with user name: '%s' does not exist", userName)));
    }
}
