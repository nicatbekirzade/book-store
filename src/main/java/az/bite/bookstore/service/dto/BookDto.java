package az.bite.bookstore.service.dto;

import az.bite.bookstore.domain.Category;
import az.bite.bookstore.domain.enums.BookStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookDto {

    private Long id;
    private String title;
    private String name;
    private String bookDescription;
    private int page;
    private BookStatus status;
    private Category category;
    private LocalDateTime creationDate;
    private Set<AuthorDto> authors;
}
